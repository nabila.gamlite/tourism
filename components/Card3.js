import React from "react";
import {
    View, Image, StyleSheet
} from "react-native";
import Text from "../constants/TextComponent";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const Card3 = props => (
        <View style={styles.container}>
            <View style={{
                flexDirection: 'row',
            }}>
                <View style={styles.cover}>
                    <Image style={styles.image} source={props.image}/>
                </View>
                <View style={{marginLeft: 5}}>
                    <View style={styles.wrapper1}>
                        <Text style={styles.title}>{props.title}</Text>
                        <MaterialIcons name="favorite-border" color="#989898" size={19}/>
                    </View>

                    <View style={{flexDirection: 'row',marginTop: 10}}>
                        <MaterialIcons name="location-pin" color="#989898" size={16}/>
                        <Text style={styles.caption}>{props.caption}</Text>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 10}}>

                        <View style={styles.wrapper1}>
                            <MaterialIcons name="king-bed" color="#989898" size={18} style={{paddingRight: 10}}/>
                            <Text style={styles.details}>{props.bed}</Text>
                        </View>
                        <View style={styles.wrapper1}>
                            <MaterialIcons name="bathtub" color="#989898" size={18} style={{paddingRight: 10}}/>
                            <Text style={styles.details}>{props.bath}</Text>
                        </View>
                        <Text style={styles.subtitle}>{props.subtitle}</Text>
                    </View>
                </View>
            </View>

        </View>
    )
;

export default Card3;

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: 310,
        height: 100,
        borderRadius: 14,
        marginVertical: 20,
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    content: {
        paddingLeft: 20,
        flexDirection: 'row',
        alignItems: 'center',
        height: 80,
        elevation: 10
    },
    cover: {
        width: '40%',
        height: '100%',
        borderRadius: 4,
        overflow: 'hidden',
        backgroundColor: 'blue'
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontWeight: 'bold',
        // marginTop: 20,
        // marginHorizontal: 10,
    },
    details: {
        color: '#909090',
        fontSize: 14,
        paddingRight: 10
        // marginTop: 20,
        // marginHorizontal: 10,
    },
    caption: {
        color: '#909090',
        fontSize: 14,
    },
    subtitle: {
        color: '#d9405a',
        fontSize: 16,
        fontWeight: 'bold',
        // marginLeft: 10,
    },
    image: {
        width: '100%',
        height: '100%',
        // position: 'absolute',
        // top: 0,
        // left: 0,
    },
    wrapper: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 10
    },
    wrapper1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    }
});
