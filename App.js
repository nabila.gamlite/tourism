import React, {useState, useEffect} from 'react';
import * as Font from 'expo-font';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk';

import AppNavigator from "./navigation/AppNavigator";
import AppLoading from 'expo-app-loading';


const fetchFonts = () => {
    return Font.loadAsync({
        'Calibri-Regular': require('./assets/fonts/Calibri-Regular.ttf'),
        'Calibri-Bold': require('./assets/fonts/Calibri-Bold.ttf'),
    });
};

export default function App() {
    const [fontLoaded, setFontLoaded] = useState(false);

    if (!fontLoaded) {
        return <AppLoading
            startAsync={fetchFonts}
            onFinish={() => setFontLoaded(true)}
            onError={console.warn}/>;
    }

    return (

            <AppNavigator/>
    );
}
