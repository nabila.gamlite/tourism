import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    screen: {
        flex: 1
    },
    gradient: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    center : {
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 8,
        elevation: 5,
        borderRadius: 10,
        backgroundColor: 'white',
    },
    authContainer: {
        width: '90%',
        maxWidth: 400,
        minHeight: 100,
        marginTop: 20,
        // marginBottom: 10,
        padding: 20,
        backgroundColor: 'white'
    },
});
