export default {
    images: [
        {
            image: require("../assets/background1.jpg"),
        },
        {
            image: require("../assets/background2.jpg"),
        },
        {
            image: require("../assets/background3.jpg"),
        },
        {
            image: require("../assets/background4.jpg"),
        },
        {
            image: require("../assets/background5.jpg"),
        },
        {
            image: require("../assets/background6.jpg"),
        },
        {
            image: require("../assets/background7.jpg"),
        },
        {
            image: require("../assets/background8.jpg"),
        },
        {
            image: require("../assets/background9.jpg"),
        },
        {
            image: require("../assets/background10.jpg"),
        },
        {
            image: require("../assets/background11.jpg"),
        },
        {
            image: require("../assets/background12.jpg"),
        },
        {
            image: require("../assets/background13.jpg"),
        },
        {
            image: require("../assets/background14.jpg"),
        },
        {
            image: require("../assets/background15.jpg"),
        },
        {
            image: require("../assets/background16.jpg"),
        }
    ],
    welcome: [
        {
            image: require("../assets/Group1013.png"),
        },
        {
            image: require("../assets/Group13363.png"),
        },
        {
            image: require("../assets/Group13630.png"),
        },
        {
            image: require("../assets/Group13632.png"),
        },
        {
            image: require("../assets/Group13633.png"),
        },
        {
            image: require("../assets/Group13634.png"),
        },
        {
            image: require("../assets/Group13635.png"),
        },
        {
            image: require("../assets/Group13636.png"),
        },
        {
            image: require("../assets/Group13637.png"),
        },

    ],
    avatar: require("../assets/avatar6.png")
}
