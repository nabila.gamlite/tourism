export default {
    midnightCalm : ['#602765','#67759A'],
    maronese : ['#C03365','#632874'],
    sepiaFond : ['#BE3A6B','#FEB06C'],
    dawn : ['#B9C4FF','#00A0D2'],
    marianaTrench : ['#B9C4FF','#00A0D2'],
    cyberpunk : ['#3B3E8A','#3498FD'],
    maybelline : ['#E3D1E8','#FBD8EA'],
    loreal : ['#FBD7E9','#F088A8'],
    tropicalOrange : ['#FF8C1C','#FFD01C'],
    seasonalTangerine : ['#FFA52F','#EB4F2C'],
    morningHaze : ['#8D8DC2','#E0D1E8'],
    sandyShore : ['#B1CCFA','#E2B7C0'],
    tifannyLove : ['#63CCCA','#CDF4E2'],
    magneticScent : ['#F6c3D0','#DE68B9'],
    brownella : ['#5B3139','#DEA7A8'],
    softResistance : ['#C3F8F8','#F88EA7'],
}
