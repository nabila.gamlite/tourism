import React, {useState} from 'react';
import {StyleSheet, Text, View, ScrollView, Dimensions, Picker} from "react-native";
import Card2 from "../components/Card2";
import Card3 from "../components/Card3";
import imageList from "../constants/Images";
import { Searchbar } from 'react-native-paper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DropDownPicker from 'react-native-dropdown-picker';
import SelectDropdown from 'react-native-select-dropdown';
import FontAwesome from "react-native-vector-icons/FontAwesome";

const dimensions = Dimensions.get("window");
const imageWidth = dimensions.width - 100;
const array = [
    {
        "id":1,
        "title": "Zila Homes",
        "location": "Bandung, Indonesia",
        "price": "$80,000/Year",
        "bed": 4,
        "bath": 3
    },
    {
        "id":2,
        "title": "Zila Homes1",
        "location": "Kuala Lumpur, Malaysia",
        "price": "$90,000/Year",
        "bed": 4,
        "bath": 5
    },
    ];
const countries = ["Egypt", "Canada", "Australia", "Ireland"]

const MainScreen = () => {
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState([
        {label: 'Indonesia', value: 'indonesia'},
        {label: 'Malaysia', value: 'malaysia'}
    ]);

    return (
        <View style={styles.rootView}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between',}}>
                <SelectDropdown
                    data={countries}
                    onSelect={(selectedItem, index) => {
                        console.log(selectedItem, index)
                    }}
                    defaultButtonText={"Location"}
                    buttonTextAfterSelection={(selectedItem, index) => {
                        // text represented after item is selected
                        // if data array is an array of objects then return selectedItem.property to render after item is selected
                        return selectedItem
                    }}
                    rowTextForSelection={(item, index) => {
                        // text represented for each item in dropdown
                        // if data array is an array of objects then return item.property to represent item in dropdown
                        return item
                    }}
                    buttonStyle={styles.dropdown1BtnStyle}
                    buttonTextStyle={styles.dropdown1BtnTxtStyle}
                    renderDropdownIcon={(isOpened) => {
                        return (
                            <FontAwesome
                                name={isOpened ? "chevron-up" : "chevron-down"}
                                color={"#444"}
                                size={18}
                            />
                        );
                    }}
                />
                <MaterialCommunityIcons name="bell-outline" color="#989898" size={28}/>
            </View>

            {/*<MaterialIcons name="location-pin" color="#d9405a" size={23}/>*/}

            <Searchbar
                placeholder="Search Property"
                style={{marginTop: 20}}
            />
            {array.length>0 && <Text style={[styles.subTitle, {color: "black"}]}>Popular</Text>}
            {array.length>0 && <ScrollView
                horizontal={true}
                style={{paddingBottom: 30}}
                showsHorizontalScrollIndicator={false}
            >
                {array.map((card, index) => (
                        <Card2
                            key={card.id}
                            title={card.title}
                            image={imageList.images[Math.floor(Math.random() * imageList.images.length)].image}
                            caption={card.location}
                            subtitle={card.price}
                            bed={card.bed}
                            bath={card.bath}
                        />
                ))}
            </ScrollView>
            }
            {array.length>0 && <Text style={[styles.subTitle, {color: "black"}]}>Recommended for You</Text>}
            {array.length>0 && <ScrollView
                horizontal={true}
                style={{paddingBottom: 30}}
                showsHorizontalScrollIndicator={false}
            >
                {array.map((card, index) => (
                    <Card3
                        key={card.id}
                        title={card.title}
                        image={imageList.images[Math.floor(Math.random() * imageList.images.length)].image}
                        caption={card.location}
                        subtitle={card.price}
                        bed={card.bed}
                        bath={card.bath}
                    />
                ))}
            </ScrollView>
            }
        </View>
    );
};

const styles = StyleSheet.create({
    rootView: {
        backgroundColor: 'white',
        flex: 1,
        paddingTop: 50,
        padding: 10
    },
    container: {
        flex: 1,
        backgroundColor: '#f0f3f5',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },
    title: {
        fontSize: 18
    },
    name: {
        fontSize: 18,
    },
    titleBar: {
        width: '100%',
        marginTop: 30,
        paddingLeft: 80,
    },
    avatar: {
        width: 44,
        height: 44,
        backgroundColor: 'black',
        borderRadius: 22
    },
    subTitle: {
        fontSize: 28,
        marginLeft: 20,
        marginTop: 20,
        fontFamily: 'Calibri-Regular'
    },
    dropdown1BtnStyle: {
        width: "30%",
        height: 30,
        backgroundColor: "#FFF",
        // borderRadius: 8,
        // borderWidth: 1,
        // borderColor: "#444",
    },
    dropdown1BtnTxtStyle: { color: "#444", textAlign: "left", fontSize: 12, fontWeight: 'bold'},

});

export default MainScreen;
