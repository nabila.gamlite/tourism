import React, {useCallback, useEffect, useState} from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createMaterialBottomTabNavigator} from "@react-navigation/material-bottom-tabs";
import {createDrawerNavigator, DrawerContentScrollView, DrawerItem} from "@react-navigation/drawer";
import {getFocusedRouteNameFromRoute, useTheme} from '@react-navigation/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MainScreen from "../screens/MainScreen";


const BottomNavigator = Platform.OS === 'android' ? createMaterialBottomTabNavigator() : createBottomTabNavigator();
export const MainBottomNavigator = () => {
    const {colors} = useTheme();
    return (
        <BottomNavigator.Navigator
            initialRouteName="Main"
            activeColor="#e6879d"
            inactiveColor='#989898'
            barStyle={{backgroundColor: colors.white}}
            screenOptions={{
                headerShown: false
            }}
            >
            <BottomNavigator.Screen
                name="Main"
                component={MainScreen}
                options={(navigation) => ({
                    tabBarLabel: 'Main',
                    tabBarActiveTintColor: "#e6879d",
                    tabBarIcon: ({color, size}) => (
                        <MaterialIcons name="home" color="#e6879d" size={23}/>
                    )
                })}
            ></BottomNavigator.Screen>
            <BottomNavigator.Screen
                name="Chat"
                component={MainScreen}
                options={(navigation) => ({
                    tabBarLabel: 'Chat',
                    tabBarActiveTintColor: "#e6879d",
                    tabBarIcon: ({color, size}) => (
                        <MaterialIcons name="chat" color="#e6879d" size={23}/>
                    )
                })}
            ></BottomNavigator.Screen>
            <BottomNavigator.Screen
                name="Favourite"
                component={MainScreen}
                options={(navigation) => ({
                    tabBarLabel: 'Favourite',
                    tabBarActiveTintColor: "#e6879d",
                    tabBarIcon: ({color, size}) => (
                        <MaterialIcons name="favorite-border" color="#e6879d" size={23}/>
                    )
                })}
            ></BottomNavigator.Screen>
            <BottomNavigator.Screen
                name="Account"
                component={MainScreen}
                options={(navigation) => ({
                    tabBarLabel: 'Account',
                    tabBarActiveTintColor: "#e6879d",
                    tabBarIcon: ({color, size}) => (
                        <MaterialIcons name="account-circle" color="#e6879d" size={23}/>
                    )
                })}
            ></BottomNavigator.Screen>
        </BottomNavigator.Navigator>
    );
};
