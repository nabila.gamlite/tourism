import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {Provider as PaperProvider} from 'react-native-paper';
import {MainBottomNavigator} from "./MainNavigator";

const AppNavigator = () => {
    return (
        <PaperProvider>
            <NavigationContainer>
                <MainBottomNavigator/>
            </NavigationContainer>
        </PaperProvider>
    );
};

export default AppNavigator;
